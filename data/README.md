##

Raw data was obtained from: https://zenodo.org/record/4714517

According to this site:

> Data Access: The data in the research collection provided may only be used for research purposes. Portions of the data are copyrighted and have commercial value as data, so you must be careful to use it only for research purposes. Due to these restrictions, the collection is not open data. Please download the Agreement at Data Sharing Agreement and send the signed form to fakenewstask@gmail.com.

Acces request is required but for the purpose of facilitating the review task we included a compressed (parquet.zip) and preprocessed version in the `proc` directory.

