# IFT714 - Projet
## Détection de fausses nouvelles (Compétition CLEF2021, tâche 3a)

### BERTs titre et contenu

Lancer les notebooks distilbert respectifs

### Validation de faits

#### Création du dataset NewsHead (déjà créé et fourni dans le dossier dataset/newshead/)
Télécharger le dataset ici: https://github.com/google-research-datasets/NewSHead

Installer news-please avec pip : 
`pip install news-please==1.4.26`

Lancer le script newshead_sitelist_cleaner.py pour épurer la liste des sites à scrapper

Lancer le script create_newshead_sample_ds.py pour creer le dataset prêt à être entrainé avec le modele BART

#### Entrainement du modele BART (modèle déjà entrainé dans le dossier src/models/distilbart/checkpoint-81285/)

Lancer le script train_BART.py.

Le modele entrainé sera sauvegardé dans le dossier src/models/distilbart

#### Fact checking

Le fact-checking peut être lancé avec le script fact-check.py, 
la sortie sera un fichier csv contenant les validations de faits dans une nouvelle colonne.

### Agrégation et métriques finales

Lancer le notebook agregation.ipynb en s'assurant que les fichiers sont présents dans le dossier outputs