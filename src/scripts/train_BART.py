from datasets import load_dataset

from transformers import AutoTokenizer, BartTokenizer
from transformers import BartForConditionalGeneration, Trainer, TrainingArguments
from transformers import DataCollatorForSeq2Seq
import evaluate
import os
import argparse

os.environ["CUDA_VISIBLE_DEVICES"] = "0"


def preprocess_function(examples):
    """
    Preprocessing function for tokenizing inputs (title + text)
    Args:
        examples: batch of examples from dataset

    Returns: Tokenized batch

    """
    inputs = []
    for i in range(len(examples['label'])):
        inputs.append(str(examples['title'][i]) + str(examples['content'][i]))
    model_inputs = tokenizer(inputs, max_length=1024, padding=True, truncation=True)
    labels = tokenizer(text_target=examples["label"], max_length=35, padding=True, truncation=True)
    model_inputs["labels"] = labels['input_ids']
    return model_inputs


def create_arg_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument("--newshead_path", default='./../../data/newshead',
                        help='Path to newshead dataset folder')
    parser.add_argument("--model", default='sshleifer/distilbart-cnn-12-6',
                        help='Local path to model or name of transformer model to use')
    parser.add_argument("--output_dir", default='./../models/distilbart',
                        help='Output path')

    return parser.parse_args()


if __name__ == "__main__":
    args = create_arg_parser()

    datafiles = {'train': f'{args.newshead_path}/dataset_small/formatted_train.json',
                 'valid': f'{args.newshead_path}/dataset_small/formatted_val.json'}
    d = load_dataset('json', data_files=datafiles)

    # create tokenizer and map dataset with
    tokenizer = BartTokenizer.from_pretrained(args.model)
    tokenized_d = d.map(preprocess_function, batched=True, remove_columns=d['train'].column_names)

    # create model from distilbart
    model = BartForConditionalGeneration.from_pretrained(args.model, min_length=5, max_length=35)

    # dataloader
    data_collator = DataCollatorForSeq2Seq(tokenizer=tokenizer, model=model)

    # create metrics function with rouge
    def compute_metrics(eval_preds):
        metric = evaluate.load("rouge")
        predictions, labels = eval_preds
        return metric.compute(predictions=predictions, references=labels)

    # create trainer with training arguments and train
    # trained model will be located in output_dir / <num_steps>
    training_args = TrainingArguments(output_dir=args.output_dir,  # output directory
                                      num_train_epochs=5,  # total number of training epochs
                                      per_device_train_batch_size=2,  # batch size per device during training
                                      per_device_eval_batch_size=1,  # batch size for evaluation
                                      warmup_steps=200,  # number of warmup steps for learning rate scheduler
                                      weight_decay=0.01,  # strength of weight decay
                                      logging_dir=f'{args.output_dir}/logs',  # directory for storing logs
                                      logging_steps=1000,
                                      save_strategy='epoch',
                                      report_to='tensorboard',
                                      )

    trainer = Trainer(model=model,
                      args=training_args,
                      train_dataset=tokenized_d['train'],
                      eval_dataset=tokenized_d['valid'],
                      tokenizer=tokenizer,
                      data_collator=data_collator,
                      compute_metrics=compute_metrics,
                      )

    trainer.train()


