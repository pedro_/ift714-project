import argparse
import traceback

import pandas as pd
import numpy as np
import torch.nn
from transformers import pipeline
import requests
import json


def open_mapping_file(path):
    mappings = {}
    with open(path, 'r') as f:
        for l in f.readlines():
            d = json.loads(l)
            mappings[next(iter(d)).strip().replace("\"", "").replace("'", "")] = d[next(iter(d))]
    return mappings


# https://support.google.com/googleapi/answer/6158862?hl=en API key
def FactCheck(query):
    payload = {
    'key': 'AIzaSyD30iX9IHS04lweLNBqKFn7RYjPG7-8qs4',
    'query':query
    }
    url = 'https://factchecktools.googleapis.com/v1alpha1/claims:search'
    response = requests.get(url, params=payload)
    if response.status_code == 200:
        result = json.loads(response.text)
        try:
            return result["claims"]
        except:
            print("No claim review field found.")
            return 0
    else:
        return 0


def fact_check_article(pipeline, title, content):
    if title:
        if len(title) > 1024:
            input_text = title[:1024]
        else:
            input_text = title + content[:1024 - len(title)]
    else:
        input_text = content[:1024]
    out = pipeline(input_text)
    print(out)

    return FactCheck(out[0]['summary_text'])


def create_arg_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument("--input_data_path", default='./../../data/outputs/test_with_results_text-CASED.csv',
                        help='Path to input csv file with titles and articles to be fact checked')
    parser.add_argument("--mapping_path", default='./../../data/proc/ratings_to_cat_annotated.jsonl',
                        help='Path to mapping file of Google API outputs to classes')
    parser.add_argument("--model_checkpoint_path", default='./../models/distilbart/checkpoint-81285/',
                        help='Path to model checkpoint')
    parser.add_argument("--text_ratings_path", default='./../../data/proc/ratings_to_cat.jsonl',
                        help='Path for output of textual ratings to be manually annotated after '
                             '(becomes --mapping_path input when annotated')
    parser.add_argument("--output_path", default='./../../data/outputs/test_with_results_factcheck.csv',
                        help='Path to output of fact-checking (filled in version of --input_data-path')

    return parser.parse_args()


if __name__ == '__main__':
    args = create_arg_parser()

    mappings = open_mapping_file(args.mapping_path)

    # file from output of BERT training, test cases, with logits included
    df = pd.read_csv(args.input_data_path)

    # set fact-check to not found in dataframe by default
    df['fact-check'] = -1

    # number of fact-checks found with both title and summary
    count = 0
    count_title = 0

    # open summarizer model checkpoint with pipeline class from huggingface
    summarizer = pipeline("summarization", args.model_checkpoint_path)

    # open similarity model directly from huggingface
    from sentence_transformers import SentenceTransformer, util
    similarity_model = SentenceTransformer('sentence-transformers/all-MiniLM-L6-v2')

    # list of TextualRatings, can be used to create mapping dict
    text_ratings = []

    for idx in range(len(df)):
        print(f"** {idx} **")
        print(df.iloc[idx]['title'])
        print(df.iloc[idx]['text'][:1024])
        print(df.iloc[idx]['rating'])

        try:
            check = FactCheck(df.iloc[idx]['title'])
            mapped_rating = -1
            if check != 0:
                count_title += 1
                print(check[0])
                embedding_1 = similarity_model.encode(check[0]['text'], convert_to_tensor=True)
                embedding_2 = similarity_model.encode(df.iloc[idx]['title'], convert_to_tensor=True)

                if util.pytorch_cos_sim(embedding_1, embedding_2).cpu().detach().numpy()[0][0] > 0.5:
                    print('SIMILAR TITLE')
                    text_ratings.append(check[0]["claimReview"][0]["textualRating"])
                    mapped_rating = mappings[
                        check[0]["claimReview"][0]["textualRating"].strip().replace("\"", "").replace("'", "")]
                else:
                    print("Not SIMILAR TITLE")
            else:
                check = fact_check_article(summarizer, df.iloc[idx]['title'], df.iloc[idx]['text'])
                if check != 0:
                    max_sim = 0
                    count += 1
                    embedding_1 = similarity_model.encode(check[0]['text'], convert_to_tensor=True)
                    for c in check:
                        embedding_2 = similarity_model.encode(df.iloc[idx]['title'], convert_to_tensor=True)
                        sim = util.pytorch_cos_sim(embedding_1, embedding_2).cpu().detach().numpy()[0][0]
                        if sim > 0.7 and sim > max_sim:
                            print(c)
                            print('MOST SIMILAR')
                            max_sim = sim
                            text_ratings.append(check[0]["claimReview"][0]["textualRating"])
                            mapped_rating = mappings[
                                check[0]["claimReview"][0]["textualRating"].strip().replace("\"", "").replace("'", "")]
            print('\n')
        except:
            traceback.print_exc()
            pass

        df.iloc[idx, df.columns.get_loc('fact-check')] = mapped_rating

    print(f"Found {count} from summaries")
    print(f"Found {count_title} from titles")
    print(f"Found {count+count_title} total")

    # save output dataframe with new filled in column
    df.to_csv(args.output_path)

    # save textual ratings to be manually annotated
    text_ratings = list(set(text_ratings))
    if args.text_ratings_path:
        with open(args.text_ratings_path, 'a') as f:
            for t in text_ratings:
                t = t.replace("\"", "'")
                f.write(f"""{{"{t} ":  }}\n""")


