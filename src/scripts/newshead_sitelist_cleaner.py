import json
import argparse


def create_arg_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument("--newshead_path", default='./../../data/newshead',
                        help='Path to newshead dataset folder')

    return parser.parse_args()


if __name__ == "__main__":
    """
    This script cleans out newshead config files to use only a subset of the urls from the entire dataset
    This version uses only the first url from each entry, rather than 5 for each headline
    """
    args = create_arg_parser()

    all_urls = []
    with open(f'{args.newshead_path}/dataset/train.json', 'r') as f:
        train = json.load(f)

        for entry in train:
            entry['urls'] = [entry['urls'][0]]
            all_urls += entry['urls']
    print(len(all_urls))
    with open(f'{args.newshead_path}/dataset/valid.json', 'r') as f:
        train = json.load(f)

        for entry in train:
            entry['urls'] = [entry['urls'][0]]
            all_urls += entry['urls']
    print(len(all_urls))
    with open(f'{args.newshead_path}/dataset/test.json', 'r') as f:
        train = json.load(f)

        for entry in train:
            entry['urls'] = [entry['urls'][0]]
            all_urls += entry['urls']

    print(len(all_urls))
    all_urls = set(all_urls)
    print('starting sitelist')

    # only keep scraping urls we use
    with open(f'{args.newshead_path}/dataset/newsplease/sitelist.hjson', 'r') as f:
        sitelist = json.load(f)
        count = 0
        i = 0
        nb = 0
        for crawler in sitelist['base_urls']:
            i += 1
            new_urls = []
            for url in crawler['url']:
                if url in all_urls:
                    new_urls += [url]
                else:
                    count += 1
                nb += len(new_urls)
            crawler['url'] = new_urls
        print(nb)
        # overwrite sitelist with smaller version
        with open(f'{args.newshead_path}/dataset_small/newsplease/sitelist.hjson', 'w') as out:
            json.dump(sitelist, out)

