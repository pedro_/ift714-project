import json
from pathlib import Path
import argparse


def create_arg_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument("--newshead_path", default='./../../data/newshead',
                        help='Path to newshead dataset folder')

    return parser.parse_args()


if __name__ == "__main__":
    """
    This script creates a formatted dataset from temporary files created by newsplease scrapper
    """
    args = create_arg_parser()

    d = {'train' : {}, 'val': {}}
    with open(f'{args.newshead_path}/dataset_small/train.json', 'r') as f:
        train = json.load(f)
        for entry in train:
            for url in entry['urls']:
                d['train'][url] = {'label': entry['label']}
    with open(f'{args.newshead_path}/dataset_small/test.json', 'r') as f:
        test = json.load(f)
        for entry in test:
            for url in entry['urls']:
                d['val'][url] = {'label': entry['label']}
    with open(f'{args.newshead_path}/dataset_small/valid.json', 'r') as f:
        val = json.load(f)
        for entry in val:
            for url in entry['urls']:
                d['val'][url] = {'label': entry['label']}

    # newsplease automatically writes to /tmp/nhnet
    path_articles = Path('/tmp/nhnet/')
    for k in d.keys():
        print(f'Looking through paths in {k}')
        missing = 0

        for path in path_articles.rglob("*.html.json"):
            with open(path, 'r') as f:
                data = json.load(f)

                try:
                    d[k][data['url']]['title'] = data['title']
                    d[k][data['url']]['content'] = data['maintext']
                except:
                    missing += 1
        print(missing)

        for k_ in list(d):
            if 'title' not in d[k][k_]:
                del d[k][k_]

        print(len(d[k]))

        # format dataset from bart
        for k_ in list(d):
            entry = d[k][k_]
            entry['url'] = k_
            with open(f'{args.newshead_path}/dataset_small/formatted_val.json', 'a') as n:
                n.write(json.dumps(entry))

