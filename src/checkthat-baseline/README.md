# Author: Mina Schütz 
# Affilliation: Darmstadt University for Applied Sciences and Austrian Institute of Technology GmbH
# E-Mail: mina.schuetz@h-da.de / mina.schuetz@ait.ac.at



# Model description
The model used for the checkthat 2022 baseline is a standard bert-base-cased model from HuggingFace (no lower-casing during training). The downloaded pre-trained model is originally trained on English data and was fine-tuned on the 900 articles from the checkthat trainingset. The training parameters were:

    - batch size: 8
    - maximum sequence length: 512
    - epochs: 10
    - learning rate: 3e-5

Since only one article was longer than the maximum sequence length, we did not use passage classification or windows for training. We used AdamW as an optimizer with a linear scheduler without warmup. For the training / validation split we used 90% of the trainingset for training and 10% for validation. The training loss was 0.04 after 10 epochs. The outputs on the validation set after completion of training were the following:

    - Accuracy: 0.56
    - Precision: 0.44 (macro-averaged)
    - Recall: 0.44 (macro-averaged)
    - F1: 0.42 (macro-averaged)

The results on the testset were the following:

    - Accuracy: 0.50
    - Precision: 0.49 (macro-averaged)
    - Recall: 0.48 (macro-averaged)
    - F1: 0.46 (macro-averaged)
 
The proposed baseline model is trained on the title and text content of the articles. This is based on former work in Schütz et al. 2021, where also a BERT-base-cased was used on another fake news detection dataset. It was shown that using the titles in front of the body content boosted the accuracy. This could also be shown on the checkthat data. The results without using the titles on the testset were:

    - Accuracy: 0.45
    - Precision: 0.45 (macro-averaged)
    - Recall: 0.43 (macro-averaged)
    - F1: 0.42 (macro-averaged)


# Cited Paper:

@incollection{epub44959,
          series = {Schriften zur Informationswissenschaft},
       publisher = {Werner H{\"u}lsbusch},
            note = {Gerhard Lustig Award Papers},
          author = {Mina Sch{\"u}tz},
         address = {Gl{\"u}ckstadt},
           title = {Detection and Identification of Fake News: Binary Content Classification with Pre-trained Language Models},
            year = {2021},
           pages = {422--431},
       booktitle = {Information between Data and Knowledge},
          volume = {74},
        keywords = {fake news; fake news detection; BERT; transformer; pre-trained language model; binary classification},
             url = {https://epub.uni-regensburg.de/44959/}
}
