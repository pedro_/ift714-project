
import numpy as np
import pandas as pd

from numpy.typing import NDArray
from typing import Any, Iterable, List, Tuple, Union

from itertools import chain
import random

import torch
from torch.utils.data import TensorDataset, random_split, DataLoader, SequentialSampler, RandomSampler
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score

import time
import datetime



# ------------------------------------------------------------------------------
#  Private functions
# ------------------------------------------------------------------------------

def _format_time(elapsed):
    """
    Takes a time in seconds and returns a string hh:mm:ss
    """
    # Round to the nearest second.
    elapsed_rounded = int(round((elapsed)))
    
    # Format as hh:mm:ss
    return str(datetime.timedelta(seconds=elapsed_rounded))



def _encode_sentences(data: 'NDArray', tokenizer: 'Any') -> 'Tuple[torch.Tensor, torch.Tensor, torch.Tensor]':
    """

    Helper function.

    Args:
        data:
        tokenizer:

    Return:
        (input_ids, attention_masks, sentence_ids): Tuple of tensors
    """

    # Tokenize all of the sentences and map the tokens to thier word IDs.
    input_ids = []
    attention_masks = []
    sentence_ids = []
    counter = 0

    # For every sentence...
    # `encode_plus` will:
    #   (1) Tokenize the sentence.
    #   (2) Prepend the `[CLS]` token to the start.
    #   (3) Append the `[SEP]` token to the end.
    #   (4) Map tokens to their IDs.
    #   (5) Pad or truncate the sentence to `max_length`
    #   (6) Create attention masks for [PAD] tokens.
    for sent in data:
        encoded_dict = tokenizer.encode_plus(
                                sent,                      # Sentence to encode.
                                add_special_tokens = True, # Add '[CLS]' and '[SEP]'
                                padding='max_length',       # pad and truncate to the max of the model (512)
                                truncation=True,
                                pad_to_max_length = True,
                                return_attention_mask = True,   # Construct attn. masks.
                                return_tensors = 'pt',     # Return pytorch tensors.
                                )
        
        # Add the encoded sentence to the list.    
        input_ids.append(encoded_dict['input_ids'])
        
        # And its attention mask (simply differentiates padding from non-padding).
        attention_masks.append(encoded_dict['attention_mask'])
        
        # collecting sentence_ids
        sentence_ids.append(counter)
        counter  = counter + 1


    # Convert the lists into tensors.
    input_ids = torch.cat(input_ids, dim=0)
    attention_masks = torch.cat(attention_masks, dim=0)
    sentence_ids = torch.tensor(sentence_ids)

    return input_ids, attention_masks, sentence_ids



# ------------------------------------------------------------------------------
#  Public functions
# ------------------------------------------------------------------------------

def create_data_loader(X_data: 'NDArray', y_data: 'NDArray', tokenizer: 'Any', batch_size=32, sampler=SequentialSampler) -> 'torch.utils.data.DataLoader':
    """


    Args:
        X_data (numpy.array):
        y_data (numpy.array):
        tokenizer (Any): The model tokenizer to use on `X_data` and `y_data`
        batch_size (int): The DataLoader needs to know our batch size for training. For fine-tuning BERT on a
            specific task, the authors recommend a batch size of 16 or 32.
        sampler (torch.Sampler): 

    Returns:
        torch.DataLoader:
    """
    input_ids, attention_masks, sentence_ids = _encode_sentences(X_data, tokenizer)
    labels = torch.tensor(y_data)

    # tensor_dataset = TensorDataset(sentence_ids, input_ids, attention_masks, labels)
    tensor_dataset = TensorDataset(input_ids, attention_masks, labels)

    # Create the DataLoaders for our training and validation sets.
    # We'll take training samples in random order. 
    data_loader = DataLoader(tensor_dataset,  # The training samples.
                            sampler=sampler(tensor_dataset), # Select batches randomly
                            batch_size=batch_size # Trains with this batch size.
                            )
    return data_loader


def _train(model: 'Any', optimizer, scheduler, training_data_loader, device, weight: 'Union[torch.Tensor,None]'=None):
    """
    Args
        weight: Weights for the C classes. Indexes of the classes [0..C]
    """
    # Put the bert_model into training mode. Don't be mislead--the call to
    # `train` just changes the *mode*, it doesn't *perform* the training.
    # `dropout` and `batchnorm` layers behave differently during training vs. test
    # (source: https://stackoverflow.com/questions/51433378/what-does-bert_model-train-do-in-pytorch)
    model.train()

    # Measure how long the training epoch takes.
    t0 = time.time()

    training_accuracy = []
    total_train_loss = 0
    ### DBG
    training_accuracy2 = []
    ### ---

    weight = weight.to(device)

    # For each batch of training data
    for step, batch in enumerate(training_data_loader):

        # Progress update. Print only 5 updates.
        if step % (len(training_data_loader) / 5) == 0 and not step == 0:
            # Calculate elapsed time in minutes.
            elapsed = _format_time(time.time() - t0)
            
            # Report progress.
            print(f' Batch {step:>5,}  of  {len(training_data_loader):>5,}.    Elapsed: {elapsed:}.')
            ### DBG
            # print(f" training_accuracy[{step-1}]:  {training_accuracy[step-1]:.2f}")
            # print(f" training_accuracy2[{step-1}]: {training_accuracy2[step-1]:.2f}")
            ### ---

        # Unpack this training batch from our dataloader. 
        #
        # As we unpack the batch, we'll also copy each tensor to the GPU using the 
        # `to` method.
        #
        # `batch` contains three pytorch tensors:
        #   [0]: input ids 
        #   [1]: attention masks
        #   [2]: labels 
        # NOTE: - `batch_size` could be less if it's the last iteration.
        #       - `embedding_size` = 512
        b_input_ids = batch[0].to(device)   # `torch.Size([batch_size, embedding_size])`
        b_input_mask = batch[1].to(device)  # `torch.Size([batch_size, embedding_size])`
        b_labels = batch[2].to(device)      # `torch.Size([batch_size])`

        # Always clear any previously calculated gradients before performing a
        # backward pass. PyTorch doesn't do this automatically because 
        # accumulating the gradients is "convenient while training RNNs". 
        # (source: https://stackoverflow.com/questions/48001598/why-do-we-need-to-call-zero-grad-in-pytorch)
        model.zero_grad()

        # Perform a forward pass (evaluate the bert_model on this training batch).
        # The documentation for this `bert_model` function is here: 
        # https://huggingface.co/transformers/v2.2.0/bert_model_doc/bert.html#transformers.BertForSequenceClassification
        # It returns different numbers of parameters depending on what arguments
        # are given and what flags are set. For our usage here, it returns
        # the loss (because we provided labels) and the "logits"--the bert_model
        # outputs prior to activation.
        # loss, logits = bert_model(b_input_ids, 
        #                     #  token_type_ids=None,
        #                      attention_mask=b_input_mask, 
        #                      labels=b_labels)
        outputs = model(b_input_ids, 
                    #  token_type_ids=None,
                    attention_mask=b_input_mask, 
                    labels=b_labels)

        # Deal with imbalanced datasets by assigning weights
        if isinstance(weight, torch.Tensor):
            outputs.logits
            criterion = torch.nn.CrossEntropyLoss(weight=weight)
            loss = criterion(outputs.logits, b_labels)
        else:
            loss = outputs.loss


        # res.logits: `torch.Size([batch_size, nb_classes])`
        preds = torch.argmax(outputs.logits, dim=-1)    # `torch.Size([batch_size])`
        accuracy_batch = torch.sum(preds == b_labels) / b_labels.size(dim=0)
        training_accuracy.append(accuracy_batch)

        ### DBG
        # print(f"b_input_ids.size(): {b_input_ids.size()}")
        # print(f"b_input_mask.size(): {b_input_mask.size()}")
        # print(f"b_labels.size(): {b_labels.size()}")

        # print(f"res.logits.size(): {res.logits.size()}")
        # print(f"preds.size(): {preds.size()}")
        ### ---

        # Accumulate the training loss over all of the batches so that we can
        # calculate the average loss at the end. `loss` is a Tensor containing a
        # single value; the `.item()` function just returns the Python value 
        # from the tensor.
        total_train_loss += loss.item()

        ### DBG 
        # Move logits and labels to CPU
        logits = outputs.logits.detach().cpu().numpy()
        label_ids = b_labels.to('cpu').numpy()

        # Calculate the accuracy for this batch of test sentences, and
        # accumulate it over all batches.
        # total_eval_accuracy += flat_accuracy(logits, label_ids)
        pred_flat = np.argmax(logits, axis=1).flatten()
        labels_flat = label_ids.flatten()
        total_eval_accuracy = np.sum(pred_flat == labels_flat) / len(labels_flat)
        training_accuracy2.append(total_eval_accuracy)

        # print(f"  len(batch):           {len(batch)}")
        # print(f"  b_labels.size(dim=0): {b_labels.size(dim=0)}")
        # print(f"  len(labels_flat):     {len(labels_flat)}")
        # print(f"  pred_flat == preds:   {(pred_flat == preds.to('cpu').numpy()).all()}")
        ### ---

        # Perform a backward pass to calculate the gradients.
        loss.backward()

        # Clip the norm of the gradients to 1.0.
        # This is to help prevent the "exploding gradients" problem.
        torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)

        # Update parameters and take a step using the computed gradient.
        # The bert_optimizer dictates the "update rule"--how the parameters are
        # modified based on their gradients, the learning rate, etc.
        optimizer.step()

        # Update the learning rate.
        scheduler.step()

    # ---
    mean_epoch_train_acc = torch.mean(torch.stack(training_accuracy))
    # ---
    
    # NOTE: `len(training_data_loader)` is the number of batches. It's equal to `len(training_accuracy)`
    ### DBG
    # print(f" training_accuracy:\n{training_accuracy}")
    # print(f" torch.stack(training_accuracy)\n{torch.stack(training_accuracy)}")
    # print(f" len(training_accuracy2):   {len(training_accuracy2)}")
    # print(f" len(training_data_loader): {len(training_data_loader)}")

    # Report the final accuracy for this validation run.
    # avg_train_accuracy = np.sum(training_accuracy2) / len(training_data_loader)
    ### ---

    # Calculate the average loss over all of the batches.
    mean_epoch_train_loss = total_train_loss / len(training_data_loader)    
    
    # Measure how long this epoch took.
    training_time = _format_time(time.time() - t0)

    print("")
    print("  Average training accuracy: {0:.2f}".format(mean_epoch_train_acc))
    ### DBG
    # print("  Average training accuracy: {0:.2f}".format(avg_train_accuracy))
    ### ---
    print("  Average training loss: {0:.2f}".format(mean_epoch_train_loss))
    print("  Training epcoh took: {:}".format(training_time))

    return mean_epoch_train_acc, mean_epoch_train_loss



def _validation(model, validation_data_loader, device, weight: 'Union[torch.Tensor,None]'=None):
    # Put the bert_model in evaluation mode--the dropout layers behave differently
    # during evaluation.
    model.eval()

    t0 = time.time()

    valid_accuracy = []
    valid_accuracy_2 = []   # TODO: this is just to compare if we obtain the same values

    weight = weight.to(device)

    # Evaluate data for one epoch
    for batch in validation_data_loader:
        
        # Unpack this training batch from our dataloader. 
        #
        # As we unpack the batch, we'll also copy each tensor to the GPU using 
        # the `to` method.
        #
        # `batch` contains three pytorch tensors:
        #   [0]: input ids 
        #   [1]: attention masks
        #   [2]: labels 
        b_input_ids = batch[0].to(device)
        b_input_mask = batch[1].to(device)
        b_labels = batch[2].to(device)
        
        # Tell pytorch not to bother with constructing the compute graph during
        # the forward pass, since this is only needed for backprop (training).
        with torch.no_grad():        
            # Forward pass, calculate logit predictions.
            # token_type_ids is the same as the "segment ids", which 
            # differentiates sentence 1 and 2 in 2-sentence tasks.
            # Get the "logits" output by the bert_model. The "logits" are the output
            # values prior to applying an activation function like the softmax.
            outputs = model(b_input_ids, 
                        # token_type_ids=None, 
                        attention_mask=b_input_mask,
                        labels=b_labels)
            
        # Deal with imbalanced datasets by assigning weights
        if isinstance(weight, Iterable):
            criterion = torch.nn.CrossEntropyLoss(weight=weight)
            loss = criterion(outputs.logits, b_labels)
        else:
            loss = outputs.loss
        
        # Accumulate the validation loss.
        total_eval_loss += loss.item()


        ## ------
        ## DEBUG
        preds = torch.argmax(outputs.logits, dim=-1)
        batch_accuracy = torch.sum(preds == b_labels) / b_labels.size(dim=0)
        valid_accuracy.append(batch_accuracy)
        ##

        # Move logits and labels to CPU
        logits = outputs.logits.detach().cpu().numpy()
        label_ids = b_labels.to('cpu').numpy()

        # Calculate the accuracy for this batch of test sentences, and
        # accumulate it over all batches.
        # total_eval_accuracy += flat_accuracy(logits, label_ids)
        pred_flat = np.argmax(logits, axis=1).flatten()
        labels_flat = label_ids.flatten()
        total_eval_accuracy = np.sum(pred_flat == labels_flat) / len(labels_flat)
        valid_accuracy_2.append(total_eval_accuracy)
        
    mean_epoch_valid_acc = torch.mean(torch.stack(valid_accuracy))
    # ---

    # Report the final accuracy for this validation run.
    ### DBG
    print(f" len(valid_accuracy_2): {len(valid_accuracy_2)}")
    print(f" len(validation_data_loader): {len(validation_data_loader)}")
    ### ---
    avg_val_accuracy = np.sum(valid_accuracy_2) / len(validation_data_loader)

    # Calculate the average loss over all of the batches.
    avg_val_loss = total_eval_loss / len(validation_data_loader)
    
    # Measure how long the validation run took.
    validation_time = _format_time(time.time() - t0)
    
    print("  Validation Loss: {0:.2f}".format(avg_val_loss))
    print("  Validation Accuracy:  {0:.2f}".format(mean_epoch_valid_acc))
    print("  Validation Accuracy2: {0:.2f}".format(avg_val_accuracy))
    print("  Validation took: {:}".format(validation_time))
    
    return mean_epoch_valid_acc
    


def epoch_solver(model: 'Any', optimizer, scheduler, training_data_loader, validation_data_loader=None,
                 epochs=3, random_state=None, weight: 'Union[torch.Tensor,None]'=None):
    """

    Notes
        Another way to train and handle weight is using the Trainer class:
        https://discuss.huggingface.co/t/class-weights-for-bertforsequenceclassification/1674/7
    """

    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    print(f"Using {device} device.")

    # This training code is based on the `run_glue.py` script here:
    # https://github.com/huggingface/transformers/blob/5bfcd0485ece086ebcbed2d008813037968a9e58/examples/run_glue.py#L128

    # Set the seed value all over the place to make this reproducible.
    if random_state != None:
        random.seed(random_state)
        np.random.seed(random_state)
        torch.manual_seed(random_state)
        torch.cuda.manual_seed_all(random_state)

    train_accuracy_history = []
    valid_accuracy_history = []
    loss_history = []
    
    # Measure the total training time for the whole run.
    total_t0 = time.time()

    # For each epoch...
    for epoch_i in range(0, epochs):
        
        # ========================================
        #               Training
        # ========================================
        # Perform one full pass over the training set.
        print("")
        print(f'======== Epoch {epoch_i + 1} / {epochs} ========')
        print("Training...")

        mean_epoch_train_acc, mean_epoch_loss = _train(model, optimizer, scheduler, training_data_loader, device, weight)

        loss_history.append(mean_epoch_loss)
        if device == 'cuda':
            train_accuracy_history.append(mean_epoch_train_acc.cpu())
        else:
            train_accuracy_history.append(mean_epoch_train_acc)
        
        # ========================================
        #               Validation
        # ========================================
        # After the completion of each training epoch, measure our performance on
        # our validation set.
        mean_epoch_valid_acc = 0
        if validation_data_loader != None:
            print("")
            print("Running Validation...")
            
            mean_epoch_valid_acc = _validation(model, validation_data_loader, device, weight)
            
            if device == 'cuda':
                valid_accuracy_history.append(mean_epoch_valid_acc.cpu())
            else:
                valid_accuracy_history.append(mean_epoch_valid_acc)
        # ------------------------

        print(f"(epoch {epoch_i + 1} / {epochs}) loss: {mean_epoch_loss}, train_acc: {mean_epoch_train_acc}, val_acc: {mean_epoch_valid_acc}")
        # ---

    print("")
    print("Training complete!")

    print("Total training took {:} (h:mm:ss)".format(_format_time(time.time()-total_t0)))

    return train_accuracy_history, valid_accuracy_history, loss_history



def predict(model: 'torch.Model', data_loader: 'torch.utils.data.DataLoader', df: 'pd.DataFrame'):

    print('Prediction started')

    # Set device for PyTorch
    if torch.cuda.is_available():    
        device = torch.device("cuda")
    else:
        device = torch.device("cpu")

    # Push model to device
    model.to(device)

    # Check if GPU or CPU 
    if torch.cuda.is_available():    
        model.cuda()
    else:
        print('No GPU available, using the CPU instead.')
        model.cpu()
    
    # Model to evaluation mode
    model.eval()
    
    ### TODO: do we need it? need to compare results
    # Helper variables for eval metrics
    test_accuracy = 0
    test_f1 = 0
    test_precision = 0
    test_recall = 0
    nb_test_steps = 0
    labels_arr = []
    softmax_arr = []
    logits_arr = []
    argmax_arr = []
    ###

    predictions = []
    true_labels = []
    
    for batch in data_loader:
        # Add batch to GPU
        batch = tuple(t.to(device) for t in batch)
        
        # Unpack the inputs from our dataloader
        b_input_ids, b_input_mask, b_labels = batch
        
        # Telling the model not to compute or store gradients, saving memory and 
        # speeding up prediction
        with torch.no_grad():
            # Forward pass, calculate logit predictions
            outputs = model(b_input_ids,
                            #    token_type_ids=None,
                            attention_mask=b_input_mask)

        logits = outputs[0]

        # Move logits and labels to CPU
        logits = logits.detach().cpu().numpy()
        label_ids = b_labels.to('cpu').numpy()
        
        # Store predictions and true labels
        predictions.append(logits)
        true_labels.append(label_ids)


    ### TODO: do we need it? need to compare results
        logits_arr.append(logits)

        # Softmax on logits for final class
        logits_soft_eval = torch.softmax(torch.tensor(logits), dim=1)
        logits_arg_eval = np.argmax(logits_soft_eval, axis=1)
        
        # Calculate eval metrics per batch
        tmp_test_accuracy = accuracy_score(label_ids, logits_arg_eval)
        tmp_test_precision = precision_score(label_ids, logits_arg_eval, average="macro")
        tmp_test_recall = recall_score(label_ids, logits_arg_eval, average="macro")
        tmp_test_f1 = f1_score(label_ids, logits_arg_eval, average="macro")
    
        # Add the metrics to a global variable to calculate the actual outcome after the testing
        test_accuracy += tmp_test_accuracy
        test_precision += tmp_test_precision
        test_recall += tmp_test_recall
        test_f1 += tmp_test_f1
    
        nb_test_steps += 1   
        
        # Divide the total metric with the testing steps to get the result
        # Accuracy, Precision, Recall, and F1
    for line in logits_arr:
        logits_soft_tmp = torch.softmax(torch.tensor(line), dim=1)
        logits_arg_tmp = np.argmax(logits_soft_tmp, axis=1)
        softmax_arr.append(logits_soft_tmp)
        argmax_arr.append(logits_arg_tmp)
    
    argmax_arr = list(chain.from_iterable(argmax_arr)) 
    
    new_list = []
    pred_0, pred_1, pred_2, pred_3 = [], [], [], []
    
    for line in softmax_arr:
        b = line.numpy()
        new_list.append(b)

    for line in new_list:
        for item in line:
            pred_0.append(item[0])
            pred_1.append(item[1])
            pred_2.append(item[2])
            pred_3.append(item[3])

    df["true"] = pred_0
    df["false"] = pred_1
    df["partially false"] = pred_2
    df["other"] = pred_3
    df["final_prediction"] = argmax_arr
    
    print(" ")
    print(f"  Accuracy: {test_accuracy / nb_test_steps:.2f}")
    print(" ")
    print(f"  Precision: {test_precision / nb_test_steps:.2f}")
    print(" ")
    print(f"  Recall: {test_recall / nb_test_steps:.2f}")
    print(" ")
    print(f"  F1: {test_f1 / nb_test_steps:.2f}")
    print(" ")
    print('  Testing complete.')
    print(" ")
    ### 

    return predictions, true_labels, df





def remove_index(tensor_data: 'torch.TensorDataset'):
    """
    Remove sentence ids from the tensor dataset after splitting in train and valid sets.
    """
    input_ids = []
    attention_masks = []
    labels = []
   
    for a,b,c,d in tensor_data:
        input_ids.append(b.tolist())
        attention_masks.append(c.tolist())
        labels.append(d.tolist())
        
    input_ids = torch.tensor(input_ids)
    attention_masks = torch.tensor(attention_masks)
    labels = torch.tensor(labels)
    
    final_dataset =  TensorDataset(input_ids, attention_masks, labels)
    return final_dataset
        
# check
# trial_dataset =  index_remover(dataset)
# trial_dataset[0]
# yes we were able to remove the sentence id from the data without disturbing the data format