

# Another dataset:
# https://www.kaggle.com/datasets/sumanthvrao/fakenewsdataset
# https://www.kaggle.com/datasets/mrisdal/fake-news

# Baseline:
# https://zenodo.org/record/6362498/export/hx#.ZBTJWxHMKXJ

# 
# https://www.kaggle.com/code/jaskaransingh/fake-news-classification-bert-roberta/notebook


import pandas as pd
import numpy as np


rating_codes = {0: "true", 1: "false", 2: "partially false", 3: "other"}


def prepare_data(df: 'pd.DataFrame'):
    '''
        This function changes the classes into numerical values and deletes all null values.
        We consider null string as null value.

        Params:
            df (DataFrame) = DataFrame containing the testset
        Returns:
            df (DataFrame) = Cleaned DataFrame, prepared for preprocessing
    
    '''
    print(f"Processing DataFrame...")

    if 'our rating' in df.columns:
        df.rename(columns={'our rating': 'rating'}, inplace=True)
    
    if 'public_id' in df.columns:
        df.rename(columns={'public_id': 'id'}, inplace=True)

    # Prepare `rating` column to transform it into a categorical column
    df.loc[:, 'rating'] = df.loc[:, 'rating'].str.lower()

    # Preferred way to encode the column when not all data is in a single dataset.
    df["rating"] = np.where(df["rating"] == "true", 0, df["rating"])
    df["rating"] = np.where(df["rating"] == "false", 1, df["rating"])
    df["rating"] = np.where(df["rating"] == "partially false", 2, df["rating"])
    df["rating"] = np.where(df["rating"] == "other", 3, df["rating"])
    
    # NOTE: `factorize` is simpler and easier BUT we can use it ONLY if all the data is in
    #       a single dataset. Otherwise we might end up with a different value encoding for
    #       each dataset/values.
    # rating_codes, rating_uniques = pd.factorize(df['rating'])
    # df.loc[:,'rating'] = pd.Categorical(rating_codes)

    # Replace the empty string with `np.nan`. This is used in case we make two models (one for `title` and
    # another for `text`) so we can drop the rows with nan.
    # NOTE: if we use just one model, it's better to leave it as empty string. This way we can concatenate
    # with `text` or `title`
    df.loc[:,'text'].str.strip().replace('', np.nan, inplace=True)
    df.loc[:,'title'].str.strip().replace('', np.nan, inplace=True)
    
    # Drop all rows with at least one null value
    df.dropna(inplace=True)

    return df