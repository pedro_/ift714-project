import pandas as pd
import numpy as np
from os import path, chdir, getcwd
from data_utils import prepare_data


# Get the absolute path to this file so we can get the directory and use work with
# relative paths
abs_path = path.abspath(__file__)
dir_name = path.dirname(abs_path)
# Save the current working directory
w_dir = getcwd()
chdir(dir_name)

raw_data_rel_path = "../../data/raw"
proc_data_rel_path = "../../data/proc"

# Variable to concatenate all the extra data and then concat to the CLEF2021 data set
expanded_train = None

# -----------------------------------------------------------------------------
#  Dataset: "Fake News Net"
#    Link: https://www.kaggle.com/datasets/mdepak/fakenewsnet?select=BuzzFeed_fake_news_content.csv
# -----------------------------------------------------------------------------
buzzfeed_fake_file = "BuzzFeed_fake_news_content.csv"
buzzfeed_real_file = "BuzzFeed_real_news_content.csv"
politifact_fake_file = "PolitiFact_fake_news_content.csv"
politifact_real_file = "PolitiFact_real_news_content.csv"

bf_real = pd.read_csv(path.join(raw_data_rel_path, buzzfeed_real_file), on_bad_lines='warn')
bf_fake = pd.read_csv(path.join(raw_data_rel_path, buzzfeed_fake_file), on_bad_lines='warn')
pf_real = pd.read_csv(path.join(raw_data_rel_path, politifact_real_file), on_bad_lines='warn')
pf_fake = pd.read_csv(path.join(raw_data_rel_path, politifact_fake_file), on_bad_lines='warn')

# Can't use a loop because of the pass-by-assignment thing in Python
bf_real = bf_real[['id', 'text', 'title']]
bf_real['rating'] = 'true'

bf_fake = bf_fake[['id', 'text', 'title']]
bf_fake['rating'] = 'false'

pf_real = pf_real[['id', 'text', 'title']]
pf_real['rating'] = 'true'

pf_fake = pf_fake[['id', 'text', 'title']]
pf_fake['rating'] = 'false'


# Concatenate all the data in a single dataframe
expanded_train = pd.concat([bf_real, bf_fake])
expanded_train = pd.concat([expanded_train, pf_real])
expanded_train = pd.concat([expanded_train, pf_fake])
print("Concatenated FakeNewsNet")
# extra_df.to_parquet(path.join(raw_data_rel_path, 'fakenewsnet.parquet.gzip'), compression='gzip')
# extra_df.to_csv(path.join(raw_data_rel_path, 'fakenewsnet.csv'))



# -----------------------------------------------------------------------------
#  Dataset: "Fakenews Classification Datasets"
#    Link: https://www.kaggle.com/datasets/liberoliber/onion-notonion-datasets?select=politifact_original.csv
# -----------------------------------------------------------------------------
# NOT USED
politifact_original_file = "politifact_original.csv"



# -----------------------------------------------------------------------------
#  Dataset: "Getting Real about Fake News"
#    Link: https://www.kaggle.com/datasets/mrisdal/fake-news
# -----------------------------------------------------------------------------
fake_file = "fake.csv"

# This dataset has one line with a problem, important to use `on_bad_lines='warn'``
fake_df = pd.read_csv(path.join(raw_data_rel_path, fake_file), on_bad_lines='warn')

fake_df.rename(columns={'uuid': 'id'}, inplace=True)

# Select only the the columns of interest
fake_df = fake_df.loc[:, ['id', 'text', 'title', 'type']]

# Convert all types non-`fake` to `other`
fake_df.loc[fake_df['type'] != 'fake', 'type'] = 'other'

# Convert `fake` to `false`
fake_df.loc[fake_df['type'] == 'fake', 'type'] = 'false'

# Rename the column 'type' to 'rating' for consistency across datasets.
fake_df.rename(columns={'type': 'rating'}, inplace=True)

expanded_train = pd.concat([expanded_train, fake_df])
print("Concatenated GettingRealAboutFakeNews")



# -----------------------------------------------------------------------------
#  Concatenate with CLEF training dataset
# -----------------------------------------------------------------------------
clef_train_file = "Task3a_training.csv"

clef_train = pd.read_csv(path.join(raw_data_rel_path, clef_train_file), on_bad_lines='warn')

# Some preprocessing before concatenation
clef_train.rename(columns={'our rating': 'rating'}, inplace=True)
clef_train.rename(columns={'public_id': 'id'}, inplace=True)

# Actual concatenation
expanded_train = pd.concat([clef_train, expanded_train])
print("Concatenated CLEF")

# Save the expanded dataset
# expanded_train.to_parquet(path.join(raw_data_rel_path, 'train_extra.parquet.gzip'), compression='gzip')
# expanded_train.to_csv(path.join(raw_data_rel_path, 'train_extra.tsv'), sep="\t")



# -----------------------------------------------------------------------------
#  Prepare training and testing datasets
# -----------------------------------------------------------------------------
clef_test_file = "Task3a_testing.csv"


clef_test = pd.read_csv(path.join(raw_data_rel_path, clef_test_file), on_bad_lines='warn')

expanded_train = prepare_data(expanded_train)
clef_train = prepare_data(clef_train)
clef_test = prepare_data(clef_test)

# Save the datasets to the processed directory
print("Saving processed datasets")

# compressed parquet
expanded_train.to_parquet(path.join(proc_data_rel_path, 'training_extra.parquet.gzip'), compression='gzip')
clef_train.to_parquet(path.join(proc_data_rel_path, 'training.parquet.gzip'), compression='gzip')
clef_test.to_parquet(path.join(proc_data_rel_path, 'testing.parquet.gzip'), compression='gzip')

# tsv
expanded_train.to_csv(path.join(proc_data_rel_path, 'training_extra.tsv'), sep="\t")
clef_train.to_csv(path.join(proc_data_rel_path, 'training.tsv'), sep="\t")
clef_test.to_csv(path.join(proc_data_rel_path, 'testing.tsv'), sep="\t")


# reset the working directory
chdir(w_dir)